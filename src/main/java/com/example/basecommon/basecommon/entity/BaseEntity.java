package com.example.basecommon.basecommon.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity implements EntityID<Long> {

    protected LocalDateTime createdAt;
    protected LocalDateTime updatedAt;
    protected LocalDateTime deletedAt;
    protected Long createdBy;
    protected Long updatedBy;


    @PrePersist
    public void beginCreated() {
        var now = LocalDateTime.now();
        setCreatedAt(now);
    }

    @PreUpdate
    public void beginUpdated() {
        var now = LocalDateTime.now();
        setUpdatedAt(now);
    }

    @PreRemove
    public void beginDeleted() {
        var now = LocalDateTime.now();
        setDeletedAt(now);
    }
}
