package com.example.basecommon.basecommon.entity;

public interface EntityID<T>{
    T getId();
    void setId(T id);
}
