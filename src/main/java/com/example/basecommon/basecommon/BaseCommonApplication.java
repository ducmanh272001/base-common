package com.example.basecommon.basecommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseCommonApplication.class, args);
	}

}
